export default ({
  namespaced: true,
  state: {
    activeFrame: null,
    routes: [],
    frames: [
      {
        id: '1',
        name: 'Текущий маршрут',
        component: 'FrameRouteInfo',
      },
      {
        id: '2',
        name: 'Предстоящие',
        component: 'FrameRouteList',
        filter: 'new',
      },
      {
        id: '3',
        name: 'Пройденные',
        component: 'FrameRouteList',
        filter: 'archieve',
      },
      {
        id: '4',
        name: 'Рекомендованные',
        component: 'FrameRouteList',
        filter: 'recommended',
      },
    ],
  },
  mutations: {
    SET_ACTIVE_FRAME(state, data) {
      state.activeFrame = data;
    },
  },
  actions: {
  },
});
