export default ({
  namespaced: true,
  state: {
    loading: [],
  },
  mutations: {
    SET_LOADING(state, data) {
      state.loading = data;
    },
    ADD_LOADING(state, data) {
      state.loading.push(data);
    },
    DEL_LOADING(state, data) {
      state.loading = state.loading.filter((el) => el !== data);
    },
  },
  actions: {
    addLoading({ commit }, data) {
      // show modal in App component
      commit('ADD_LOADING', data);
    },
    delLoading({ commit }, data) {
      commit('DEL_LOADING', data);
    },
  },
  getters: {
    someLoading(state) {
      return state.isAuthorized;
    },
    isLoading: (state) => (method) => {
      const regex = method instanceof RegExp ? method : new RegExp(method, 'gi');
      return state.loading.filter((l) => regex.test(l)).length > 0;
    },
  },
});
