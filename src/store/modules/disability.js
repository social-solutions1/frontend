export default ({
  namespaced: true,
  state: {
    disability: {
      contrast: false,
      large: false,
      speech: false,
    },
  },
  mutations: {
    SET_DISABILITY(state, data) {
      state.disability = data;
    },
  },
  actions: {
    setContrast({ state, commit }, contrast) {
      commit('SET_DISABILITY', { ...state.disability, contrast });
    },
    setLarge({ state, commit }, large) {
      commit('SET_DISABILITY', { ...state.disability, large });
    },
    setSpeech({ state, commit }, speech) {
      commit('SET_DISABILITY', { ...state.disability, speech });
    },
  },
});
