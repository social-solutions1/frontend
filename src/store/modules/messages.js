import { nanoid } from 'nanoid';

export default ({
  namespaced: true,
  state: {
    messages: [],
  },
  mutations: {
    SET_MESSAGES(state, data) {
      state.messages = data;
    },
    ADD_MESSAGE(state, data) {
      state.messages.push(data);
    },
    DEL_MESSAGE(state, data) {
      state.messages = state.messages.filter((el) => el.uuid !== data);
    },
  },
  actions: {
    addMessage({ commit }, data) {
      // show modal in App component
      const uuid = nanoid();
      commit('ADD_MESSAGE', {
        uuid,
        ...data,
      });
    },
    delMessage({ commit }, data) {
      commit('DEL_MESSAGE', data);
    },
  },
});
