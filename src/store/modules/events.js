import Vue from 'vue';
import Event from '../../services/events';
import EventModel from '../../models/event';
import { DEFAULT_LIMIT, DEFAULT_PAGE } from '../../consts/pagination';

export default ({
  namespaced: true,
  state: {
    list: [],
    page: DEFAULT_PAGE,
    filters: {
      areas: [],
      spheres: [],
      themes: [],
    },
  },
  mutations: {
    PUSH_LIST(state, list) {
      state.list = [...state.list, ...list];
    },
    SET_LIST_ITEM_BY_ID_AND_PROP(state, { id, key, value }) {
      const listItem = state.list.find((item) => item.id === id);
      listItem[key] = value;
    },
    RESET_LIST(state) {
      Vue.set(state, 'list', []);
    },
    SET_FILTERS(state, value) {
      state.filters = value;
    },
    SET_FILTER_BY_KEY(state, { key, value }) {
      state.filters[key] = value;
    },
    RESET_FILTERS(state) {
      Object.keys(state.filters).forEach((key) => {
        Vue.set(state.filters, key, []);
      });
    },
    GO_TO_NEXT_PAGE(state) {
      state.page += 1;
    },
    RESET_PAGE(state) {
      state.page = DEFAULT_PAGE;
    },
  },
  actions: {
    // eslint-disable-next-line no-unused-vars
    async getList({ state, commit, dispatch }) {
      const status = {
        isComplete: false,
      };

      dispatch('loading/addLoading', `${EventModel.controller}.list`, { root: true });
      try {
        const { events } = await Event.list({
          pagination: {
            page: state.page,
            limit: DEFAULT_LIMIT,
          },
          filters: state.filters,
        });
        if (events.length) {
          commit('PUSH_LIST', events);
          commit('GO_TO_NEXT_PAGE');
        } else {
          status.isComplete = true;
        }
        return status;
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
        return status;
      } finally {
        dispatch('loading/delLoading', `${EventModel.controller}.list`, { root: true });
      }
    },
    async addToFavorite({ dispatch, commit }, eventId) {
      const loaderKey = `${EventModel.controller}.favorites.update-${eventId}`;
      dispatch('loading/addLoading', loaderKey, { root: true });

      try {
        await Event.addToFavorite({
          event_id: eventId,
        });
        commit('SET_LIST_ITEM_BY_ID_AND_PROP', {
          id: eventId,
          key: 'subscribed',
          value: true,
        });
      } catch (e) {
        console.error(e);
      } finally {
        dispatch('loading/delLoading', loaderKey, { root: true });
      }
    },
    setFilters({ commit }, filters) {
      commit('RESET_LIST');
      commit('RESET_PAGE');
      commit('SET_FILTERS', filters);
    },
  },
  getters: {
    hasSomeFilter(state) {
      return Object.values(state.filters).some((values) => !!values.length);
    },
  },
});
