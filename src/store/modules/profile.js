import AuthService from '../../services/auth';
import ProfileModel from '../../models/profile';
import ProfileService from '../../services/profile';
import AuthModel from '../../models/auth';

export default ({
  namespaced: true,
  state: {
    profile: {},
    isAuthorized: false,
  },
  mutations: {
    SET_PROFILE(state, data) {
      state.profile = data;
    },
    SET_IS_AUTHORIZED(state, data) {
      state.isAuthorized = data;
    },
  },
  actions: {
    async login({ commit, dispatch }, { email, password }) {
      if (!email || !password) return Promise.reject();
      dispatch('loading/addLoading', `${AuthModel.controller}.login`, { root: true });
      return AuthService.login({ email, password })
        .then((authorized) => {
          const profile = new ProfileModel(authorized);

          const { token } = authorized;
          dispatch('setToken', token);

          commit('SET_PROFILE', profile);
          commit('SET_IS_AUTHORIZED', true);
          return profile;
        })
        .catch((error) => {
          const type = 'error';
          const { message } = error;
          dispatch('messages/addMessage', { message, type }, { root: true });
          return false;
        })
        .finally(() => {
          dispatch('loading/delLoading', `${AuthModel.controller}.login`, { root: true });
        });
    },
    async signup({ commit, dispatch }, {
      email, password, name, phone,
    }) {
      if (!email || !password) return Promise.reject();
      dispatch('loading/addLoading', `${AuthModel.controller}.register`, { root: true });
      return AuthService.register({
        email, password, name, phone,
      })
        .then((authorized) => {
          const profile = new ProfileModel(authorized);

          const { token } = authorized;
          dispatch('setToken', token);

          commit('SET_PROFILE', profile);
          commit('SET_IS_AUTHORIZED', true);
          return profile;
        })
        .catch((error) => {
          const type = 'error';
          const { message } = error;
          dispatch('messages/addMessage', { message, type }, { root: true });
          return false;
        })
        .finally(() => {
          dispatch('loading/delLoading', `${AuthModel.controller}.register`, { root: true });
        });
    },
    setToken(store, token) {
      localStorage.setItem('token', token);
    },
    logout({ commit }) {
      commit('SET_IS_AUTHORIZED', false);
      commit('SET_PROFILE', {});
      localStorage.removeItem('token');
    },
    async getProfile({ commit, getters, dispatch }) {
      const token = getters.getToken;
      if (!token) return {};
      dispatch('loading/addLoading', `${ProfileModel.controller}.me`, { root: true });
      const profile = await ProfileService.me()
        .finally(() => {
          dispatch('loading/delLoading', `${ProfileModel.controller}.me`, { root: true });
        });
      commit('SET_PROFILE', profile);
      commit('SET_IS_AUTHORIZED', true);
      return profile;
    },
    async setAdvancedInfo({ dispatch }, answers) {
      dispatch('loading/addLoading', `${ProfileModel.controller}.setAdvancedInfo`, { root: true });
      try {
        ProfileService.setAdvancedInfo(answers);
      } catch (e) {
        console.error(e);
      } finally {
        dispatch('loading/delLoading', `${ProfileModel.controller}.setAdvancedInfo`, { root: true });
      }
    },
  },
  getters: {
    isAuthorized(state) {
      return state.isAuthorized;
    },
    getToken() {
      return localStorage.getItem('token');
    },
  },
});
