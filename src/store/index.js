import Vue from 'vue';
import Vuex from 'vuex';

import disability from './modules/disability';
import profile from './modules/profile';
import messages from './modules/messages';
import events from './modules/events';
import loading from './modules/loading';
import routes from './modules/routes';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {
  },
  modules: {
    disability,
    profile,
    messages,
    events,
    loading,
    routes,
  },
});
