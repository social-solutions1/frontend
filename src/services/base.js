import { Http, request } from '../api';

export default class Base {
  static request = request;

  static updateHttp = (token) => {
    this.request = new Http({ Authorization: `Bearer ${token}` }).request;
  }
}
