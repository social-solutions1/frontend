import BaseService from './base';
import EventModel from '../models/event';

export default class Event {
  static list(params = {}) {
    return BaseService.request(EventModel.controller, 'list', params)
      .then(({ events }) => ({
        events: events.map((event) => new EventModel(event)),
      }));
  }

  // eslint-disable-next-line no-unused-vars
  static addToFavorite(params = {}) {
    return BaseService.request(EventModel.controller, 'favorites.add', params)
      .then(() => true);
  }
}
