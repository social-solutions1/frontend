import ProfileModel from '../models/profile';
import BaseService from './base';

export default class Profile extends BaseService {
  constructor(data = {}) {
    super(data);
  }

  static me() {
    return BaseService.request(ProfileModel.controller, 'me')
      .then((res) => new ProfileModel(res));
  }

  static setAdvancedInfo(params = {}) {
    return BaseService.request(ProfileModel.controller, 'setAdvancedInfo', params);
  }
}
