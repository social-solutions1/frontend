import EmailModel from '../models/email';
import BaseService from './base';

export default class Email extends BaseService {
  constructor(data = {}) {
    super(data);
  }

  static check(params = {}) {
    return BaseService.request(EmailModel.controller, 'check', params)
      .then((res) => res.found);
  }
}
