import AuthModel from '../models/auth';
import errors from '../i18n/api-errors.json';
import BaseService from './base';

export default class Auth extends BaseService {
  constructor(data = {}) {
    super(data);
  }

  static login(params = {}) {
    return BaseService.request(AuthModel.controller, 'login', params)
      .then((res) => {
        const authorized = new AuthModel(res);
        const { token } = authorized;

        BaseService.updateHttp(token);

        return authorized;
      })
      .catch((error) => {
        const codeMessage = error.message || 'unknown';
        const message = errors[codeMessage];
        throw new Error(message);
      });
  }

  static register(params = {}) {
    return BaseService.request(AuthModel.controller, 'register', params)
      .then((res) => new AuthModel(res));
  }
}
