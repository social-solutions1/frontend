import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import dayjs from './plugins/day';

Vue.config.productionTip = false;

// prototype section
Vue.prototype.$dayjs = dayjs;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
