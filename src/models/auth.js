import BaseModel from './base';

export default class AuthModel extends BaseModel {
  static controller = 'auth';

  constructor(data = {}) {
    super(data);

    this.uuid = data.uuid;
    this.token = data.token;
    this.email = data.email;
    this.phone = data.phone;
    this.name = data.name;
  }
}
