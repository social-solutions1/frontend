import BaseModel from './base';

export default class ProfileModel extends BaseModel {
  static controller = 'profile';

  constructor(data = {}) {
    super(data);

    this.email = data.email;
    this.phone = data.phone;
    this.name = data.name;
    this.uuid = data.uuid;
  }
}
