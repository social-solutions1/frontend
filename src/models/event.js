import BaseModel from './base';
import { AFISHA_API_HOST } from '../consts/dummy';

export default class EventModel extends BaseModel {
  static controller = 'events';

  constructor(data = {}) {
    super(data);

    this.id = data.id;
    this.title = data.title;
    this.free = data.free;
    this.thumb = AFISHA_API_HOST + data.image.thumb.src;
    this.subscribed = false;
  }
}
