import BaseModel from './base';

export default class EventModel extends BaseModel {
  static controller = 'email';

  constructor(data = {}) {
    super(data);
  }
}
