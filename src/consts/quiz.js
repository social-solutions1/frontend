// eslint-disable-next-line import/prefer-default-export
export const QUIZ_QUESTIONS = [
  {
    title: 'Впервые в Москве?',
    key: 'first_in_city',
  },
  {
    title: 'В каком районе Москвы Вам было бы удобнее всего посещать мероприятия?',
    key: 'areas',
  },
  {
    title: 'Выберите категории мероприятий которые Вам наиболее интересны',
    key: 'spheres',
  },
  {
    title: 'Выберите темы которые наиболее для Вас интересны',
    key: 'themes',
  },
];
