import { RequestManager, Client, HTTPTransport } from '@open-rpc/client-js';

const protocol = process.env.VUE_APP_PROTOCOL || 'https';
const host = process.env.VUE_APP_HOST || 'localhost';
let port = process.env.VUE_APP_PORT || '';
port = port ? `:${port}` : '';
const path = process.env.VUE_APP_PATH || '/rpc/';

export class Http {
  constructor(headers) {
    this.transport = new HTTPTransport(`${protocol}://${host}${port}${path}`, { headers });
    this.requestManager = new RequestManager([this.transport]);
    this.client = new Client(this.requestManager);
    this.request = (controller, method, params) => this.client.request({ method: `${controller}.${method}`, params });
  }
}

const token = localStorage.getItem('token');

const headers = {
  Authorization: token ? `Bearer ${token}` : '',
};

const client = new Http(headers);

export const { request } = client;
