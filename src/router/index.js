import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import('../pages/index.vue'),
  },
  {
    path: '/my-events',
    name: 'my-events',
    component: () => import('../pages/PageUserEvents.vue'),
    meta: { requiresAuth: true },
  },
  {
    path: '/events',
    name: 'events',
    component: () => import('../pages/PageEventsList.vue'),
  },
  {
    path: '/calendar',
    name: 'calendar',
    component: () => import('../pages/PageCalendar.vue'),
  },
  {
    path: '/routes',
    name: 'routes',
    component: () => import('../pages/route/PageRouteList.vue'),
  },
  {
    path: '/routes/build',
    name: 'routes-build',
    component: () => import('../pages/route/PageBuildRoute.vue'),
  },
  {
    path: '/routes/:id',
    name: 'routes',
    component: () => import('../pages/route/PageRouteList.vue'),
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../pages/PageLogin.vue'),
  },
  {
    path: '/user-quiz',
    name: 'user-quiz',
    component: () => import('../pages/PageUserQuiz.vue'),
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
