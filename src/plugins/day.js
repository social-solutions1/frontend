import * as dayjs from 'dayjs';
import 'dayjs/locale/ru';
import * as relativeTime from 'dayjs/plugin/relativeTime';
import * as duration from 'dayjs/plugin/duration';

dayjs.extend(duration);
dayjs.extend(relativeTime);
dayjs.locale('ru');

export default dayjs;
